# Airflow Local #

Follow the steps below to get Airflow up and running for local standalone testing and development. This repo is based off docker images provided by [bitnami.](https://bitnami.com/stack/apache-airflow/containers)

This repo is for **Airflow 1.X**, as version 2.X is not currently integrated into GCP

### Prerequisites and Notes: ###

Install/setup Docker and allocate 8GB of RAM in Docker

### Getting Started With Development: ###

Navigate to an appropriate directory and clone the Airflow Local Repo:

```python
git clone git@bitbucket.org:latxhman/pcb-onboarding-airflow.git
```

Build and start all docker containers in the docker compose yaml (this will handle installing all dependencies):

```python
docker-compose up
```

Let Docker spin up all containers (may take some time) and head to:

```
localhost:8080
```

Enter local default credentials:

```
username: user
password: bitnami
```

Once logged in, you will see many example workflows in the dashboard. If this is your first time using Airflow it is recommended to go through these workflows to gain a better understanding of Airflow.

Your local environment is now setup!